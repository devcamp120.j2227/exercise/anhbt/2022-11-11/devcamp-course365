// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import mongooseJS
const mongoose = require("mongoose");

// Import router
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

// Khởi tạo app express
const app = express();

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))
app.use(express.static(__dirname + "/views"));
app.get("/", function(request, response) {
    response.sendFile(path.join(__dirname + "/views/index.html"))
})
// Khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://127.0.0.1:27017/devcamp-course365", (err) => {
    if(err) {
        throw err;
    }

    console.log("Connect to                                      MongoDB successfully!");
})
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)


app.use("/", courseRouter);
app.use("/", reviewRouter);

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})