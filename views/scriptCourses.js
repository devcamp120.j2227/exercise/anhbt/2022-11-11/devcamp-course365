/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gCOURSE_COLS = [ "id", "courseName", "level", "price", "discountPrice", "teacherName", "action" ];
    
// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCOURSE_COLS_ID = 0;
const gCOURSE_COLS_NAME = 1;
const gCOURSE_COLS_LEVEL = 2;
const gCOURSE_COLS_PRICE = 3;
const gCOURSE_COLS_PRICE_DISCOUNTED = 4;
const gCOURSE_COLS_TEACHER = 5;
const gCOURSE_COLS_ACTION = 6;

var gTableCourses = $("#courses-table").DataTable({
    columns: [
    {   data: gCOURSE_COLS[gCOURSE_COLS_ID] },
    {   data: gCOURSE_COLS[gCOURSE_COLS_NAME] },
    {   data: gCOURSE_COLS[gCOURSE_COLS_LEVEL] },
    {   data: gCOURSE_COLS[gCOURSE_COLS_PRICE] ,
        className: "text-right",
    },
    {   data: gCOURSE_COLS[gCOURSE_COLS_PRICE_DISCOUNTED],
        className: "text-right",
     },
    {   data: gCOURSE_COLS[gCOURSE_COLS_TEACHER] },
    {   data: gCOURSE_COLS[gCOURSE_COLS_ACTION] ,
        defaultContent: `
        <div class="btn-group" role="group">
            <button class="btn btn-outline-info btn-sm edit-course">Sửa</button>
            <button class="btn btn-outline-danger btn-sm delete-course">Xoá</button>
        </div>
        `}
    ]
})

var gCourseToDeleteId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();
$("#btn-add-course").click(function() {
    $("#create-modal").modal("show");
});

$("#btn-create").click(onBtnCreateCourseClick);
$("#btn-create-cancel").click(resetModalCreate);

$("#courses-table").on("click", ".edit-course", function(){
    onBtnEditCourseClick($(this));
})
$("#btn-update").click(onBtnUpdateClick);

$("#courses-table").on("click", ".delete-course", function(){
    onBtnDeleteCourseClick($(this));
})
$("#btn-confirm-delete-course").click(onBtnConfirmDeleteCourseClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    displayCourses(gCoursesDB.courses);
}

function onBtnEditCourseClick(paramButton) {
    $("#update-modal").modal("show");

    var courseId = getCourseIdFromButton(paramButton);
    var course = getCourseById(courseId);
    loadCourseDataToUpdateModal(course);
}
function onBtnConfirmDeleteCourseClick() {
    var courseIndex = getCourseIndexById(gCourseToDeleteId);
    gCoursesDB.courses.splice(courseIndex, 1);
    displayCourses(gCoursesDB.courses);
    $("#delete-confirm-modal").modal("hide");
}
function onBtnDeleteCourseClick(paramButton) {
    gCourseToDeleteId = getCourseIdFromButton(paramButton);
    $("#delete-confirm-modal").modal("show");
}
function onBtnUpdateClick() {
    // thu thập dữ liệu
    var vUpdateCourseObj = {
        id: 1,
        courseCode: "",
        courseName: "",
        price: -1,
        discountPrice: -1,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false
    };
    getUpdateCourseData(vUpdateCourseObj);

    // kiểm tra dữ liệu
    let vDataIsValid = validateCourseData(vUpdateCourseObj);
    if(vDataIsValid == true) {
        // update course nếu dữ liệu ok
        var courseIndex = getCourseIndexById(vUpdateCourseObj.id);
        gCoursesDB.courses[courseIndex] = vUpdateCourseObj;
        displayCourses(gCoursesDB.courses);
        $("#update-modal").modal("hide");
    }
}
function onBtnCreateCourseClick() {
    // thu thập dữ liệu
    var vNewCourseObj = {
        id: 1,
        courseCode: "",
        courseName: "",
        price: -1,
        discountPrice: -1,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false
    };
    getNewCourseData(vNewCourseObj);

    // kiểm tra dữ liệu
    let vDataIsValid = validateCourseData(vNewCourseObj);
    if(vDataIsValid == true) {
        // tạo course nếu dữ liệu ok
        gCoursesDB.courses.push(vNewCourseObj);
        displayCourses(gCoursesDB.courses);
        resetModalCreate();
    }
}
/*** REGION 4 - Common functions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadCourseDataToUpdateModal(paramCourse) {
    $("#input-update-id").val(paramCourse.id);
    $("#input-update-name").val(paramCourse.courseName);
    $("#input-update-code").val(paramCourse.courseCode);
    $("#input-update-teacher").val(paramCourse.teacherName);
    $("#input-update-duration").val(paramCourse.duration);
    $("#input-update-level").val(paramCourse.level);
    $("#input-update-price").val(paramCourse.price);
    $("#input-update-discounted").val(paramCourse.discountPrice);
    $("#input-update-popular ").prop("checked",paramCourse.isPopular);
    $("#input-update-trending ").prop("checked",paramCourse.isTrending);
}
function displayCourses(paramCourses) {
    gTableCourses.clear();
    gTableCourses.rows.add(paramCourses);
    gTableCourses.draw( false );
}

function validateCourseData(paramCourse) {
    return true;
}

// hàm trả về course với tham số course id đã cho
function getCourseById(paramCourseId) {
    return gCoursesDB.courses.find(({ id }) => id == paramCourseId);
}

function getCourseIdFromButton(paramButton) {
    var vThisRow = paramButton.closest("tr");
    var vRowData = gTableCourses.row(vThisRow).data();
    return vRowData.id;
}

function getCourseIndexById(paramCourseId) {
    return gCoursesDB.courses.findIndex((course => course.id == paramCourseId));
}

function resetModalCreate() {
    $("#create-modal").find("input").val("");
    $("#input-create-popular").prop("checked", false);
    $("#input-create-trending").prop("checked", false);
    $("#create-modal").modal("hide");
}
function getNewCourseData(paramCourse) {
    paramCourse.id = getNextId();
    paramCourse.courseName  = $("#input-create-name").val().trim();
    paramCourse.courseCode  = $("#input-create-code").val().trim();
    paramCourse.teacherName = $("#input-create-teacher").val().trim();
    paramCourse.duration    = $("#input-create-duration").val().trim();
    paramCourse.level       = $("#input-create-level").val().trim();
    paramCourse.price       = $("#input-create-price").val();
    paramCourse.discountPrice = $("#input-create-discounted").val();
    paramCourse.isPopular   = $("#input-create-popular ").prop("checked");
    paramCourse.isTrending  = $("#input-create-trending ").prop("checked");
}
function getUpdateCourseData(paramCourse) {
    paramCourse.id = $("#input-update-id").val();
    paramCourse.courseName  = $("#input-update-name").val().trim();
    paramCourse.courseCode  = $("#input-update-code").val().trim();
    paramCourse.teacherName = $("#input-update-teacher").val().trim();
    paramCourse.duration    = $("#input-update-duration").val().trim();
    paramCourse.level       = $("#input-update-level").val().trim();
    paramCourse.price       = $("#input-update-price").val();
    paramCourse.discountPrice = $("#input-update-discounted").val();
    paramCourse.isPopular   = $("#input-update-popular ").prop("checked");
    paramCourse.isTrending  = $("#input-update-trending ").prop("checked");
}
// hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
function getNextId() {
    var vNextId = 0;
    // Nếu mảng chưa có đối tượng nào thì Id = 1
    if( gCoursesDB.courses.length == 0 ) {
    vNextId = 1;
    }
    // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
    else {
        vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
    }
    return vNextId;
}

